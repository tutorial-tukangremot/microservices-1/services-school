<?php

namespace App\Exceptions;

use Svknd\Laravel\Helpers\Exceptions\Handler as SvkndHandler;

class Handler extends SvkndHandler
{
    public function __construct()
    {
        $this->validationMessage = __('error.422');
        $this->modelNotFoundMessage = __('error.404');
    }
}
