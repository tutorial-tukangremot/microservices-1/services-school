<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\School;
use App\Http\Resources\School as SchoolResource;

class Controller extends BaseController
{
    public function view(int $id)
    {
        $school = School::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'success',
            'data' => new SchoolResource($school)
        ], 200);
    }
}
