<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $school = School::create([
                'id' => 1,
                'name' => 'Sekolah Tutorial'
            ]);
        } catch (\Throwable $th) {
            //
        }
    }
}
