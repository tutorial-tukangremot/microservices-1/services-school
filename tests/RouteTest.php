<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\School;

class RouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetSchoolDetail()
    {
        /**
         * start test with invalid data type
         */
        $this->get('/schools/a');

        $this->seeStatusCode(500);
        $this->seeJsonStructure([
            'success',
            'message',
        ]);
        /**
         * end test with invalid data type
         */

        /**
         * start test with invalid id
         */
        $this->get('/schools/1');

        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'success',
            'message',
        ]);
        /**
         * end test with invalid id
         */

        /**
         * start test with valid id
         */
        // create fake user
        $school = School::factory()->create([
            'name' => $this->faker->name
        ]);

        $this->get('/schools/' . $school->id);

        $this->seeStatusCode(200);
        $this->seeJson([
            'success' => true,
            'data' => [
                'id' => $school->id,
                'name' => $school->name
            ]
        ]);
        /**
         * end test with valid id
         */
    }
}
