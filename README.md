# Services School
* [Request & Response Examples](#request--response-examples)

## Request & Response Examples
### API Resources

* Detail School
    * [GET /schools/{id}](#get-schools--id--)

### GET /schools/{id}

Response body:

    {
        "success": true,
        "message": "success",
        "data": {
            "id": 1,
            "name": "Sekolah Tutorial"
        }
    }
